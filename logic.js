let blackJackGame = {
  you: { scoreSpan: "#human__result__left", div: "#human", score: 0 },
  bot: { scoreSpan: "#human__result__right", div: "#bot", score: 0 },
};

const YOU = blackJackGame["you"];
const BOT = blackJackGame["bot"];

function hitClick() {
  let cardImage = document.createElement("img");
  cardImage.src = "images/Q.png";
  document.querySelector(YOU["div"]).appendChild(cardImage);
}

function standClick() {
  console.log("Clicked STAND Button");
}

function dealClick() {
  console.log("Clicked DEAL Button");
}
